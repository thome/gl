
Simplistic python CLI for gitlab
================================

This is just a quick set of hacks. It doesn't come anywhere near more
serious tools like [glab](https://glab.readthedocs.io/) or
[lab](https://github.com/zaquestion/lab).

This tool does seem to have functionalities that are not present in other
tools, though. (disclaimer, I learned about lab and glab after the fact
anyway).

This also allows one to auto-provision runners on the French Grid5000
clusters (g5k).


Configuration (simple usage)
============================

The script looks for a file called `gl.json` in the current directory. If
a file called `gl.json.gpg` is found instead, decryption is attempted
using `gpg -d`.  The same pair of files are looked up in `$HOME` as well.
If you pass `-C`, this gives you the opportunity to pass any custom
config file.

The contents of `gl.json` should be as follows. It's json, so no
comments, no commas at the end of lists, etc.

Assume that you're John Smith, and that you work on project `smith/thing`
on `gitlab.foo.com`.

You must have a personal access token to `gitlab.foo.com` (User settings
-> Access tokens), which we'll call `PAT_PAT_PAT_PAT`.

In order to access only the `gl pipeline` or `gl runner` views (**not**
`gl runner provision`), this is basically all you need. Your `gl.json`
should look like:

```
{
    "gitlab_project": "smith%2Fthing",
    "gitlab_access_token": "PAT_PAT_PAT_PAT"
}
```

Configuration (with runner provisioning)
========================================

This section assumes that you have an account on the French Grid5000
platform.


Your runners on the project `smith/thing` register to the gitlab instance
using a registration token (Settings -> CI/CD -> Runners) which we'll
call `REGTOK_REGTOK_REGTOK`.

You Grid5000 account is probably called `jsmith`, and your password is,
let's say, `G5KP_G5KP_G5KP`.

This assumes that you can ssh to a machine in the `.grid5000.fr` domain.
There are several ways to do that. One of them is to have the [Grid5000
vpn](https://www.grid5000.fr/w/VPN). Another one is to set up your
`.ssh/config` accordingly. Documentation for this is not covered here.

The path to the ssh public keys (on your own machine) that you will copy
on the target nodes is, let's say, `/home/jsmith/.ssh/id_rsa.pub`. This
is the key that the remote machine will expect, so you'd better have it
in your ssh agent. (therefore `gl` itself is stateless, it only relies on
key material that exists outside)

Assume that you want you jobs to run on cluster `SOMECLUSTER`, on site
`SOMESITE`.

All of this put together, this is an example of what `gl.json` could look
like.

```
{
    "gitlab_project": "smith%2Fthing",
    "gitlab_access_token": "PAT_PAT_PAT_PAT",
    "gitlab_registration_token": "REGTOK_REGTOK_REGTOK",
    "docker_image": "gcc",
    "g5k_properties": {
                "properties": "cluster='SOMECLUSTER'",
                "name": "runner",
                "resources": "walltime=2"
                },
    "g5k_site": "SOMESITE",
    "ssh_public_key_files": [ "/home/jsmith/.ssh/id_rsa.pub" ]
    "submission_access_timeout": 180,
    "g5k_login": "jsmith",
    "g5k_password": "G5KP_G5KP_G5KP"
}
```

There are other possible config keywords, such as `runner_tags`
(array of strings) or `ssh_private_key`.


Installation
============

None, really. I'm too lazy. Copy this repo somewhere, maybe in `/tmp` for
a simple try, and call the `gl.py` command there, from any directory.

Commands
========

You might want to link or alias the `gl` command to this tool. The
documentation below assumes that you have done so.

 - List the existing runners for the project, and their status:
   ```
   gl runners
   ```
 
 - List the jobs on the given runners:
   ```
   gl runner jobs [RUNNER ID] [MORE RUNNER IDS ...]
   ```
 
 - Pause the given runners:
   ```
   gl runner pause [RUNNER ID] [MORE RUNNER IDS ...]
   ```
 
 - Resume the given runners:
   ```
   gl runner resume [RUNNER ID] [MORE RUNNER IDS ...]
   ```
 
 - Delete the given runners:
   ```
   gl runner delete [RUNNER ID] [MORE RUNNER IDS ...]
   ```
 
 - Give details (in json format) on the given runners. You might want to
   pipe that into the `jq` tool for coloring. Note that `glab api
   runners/[RUNNER ID]` is more or less the same.
   ```
   gl runner details [RUNNER ID] [MORE RUNNER IDS ...]
   ```
 
 - List the currently running pipelines for the project:
   ```
   gl pipeline
   ```
 
 - List the pipelines with the given status, or all pipelines if "all"
   is passed.
   ```
   gl pipeline [alphabetic string]
   ```
 
 - List the given pipelines.
   ```
   gl pipeline [PIPELINE ID] [MORE PIPELINE IDS ...]
   ```
 
 - Starts a runner according to what is found in `gl.json`
   ```
   gl runner provision
   ```

To provision several runners in parallel, I use GNU parallel as follows:
```
parallel -u --termseq INT,1000,HUP,1000,TERM,200,KILL,25 gl r prov ::: {1..4}
```

Abbreviations
=============

See source code for some abbreviations.

- `gl runner` can be abbreviated as `gl r`
- `gl runner jobs` can be abbreviated as `gl r` (`jobs` can be removed)
- `gl runner provision` can be abbreviated as `gl r prov`
- `gl runner delete` can be abbreviated as `gl r del`
- `gl pipeline` as `gl p`
