import base64
import json
import logging
import netrc
import os
import re
import requests
import subprocess
import sys
import time
import uuid
from gl.no_boredom import no_boredom
from urllib.parse import urlparse,urlunparse

from gl.eye_candy import ScreenHandler

class gl_api(no_boredom):
    def __init__(self,
            gitlab_base="https://gitlab.inria.fr/api/v4",
            gitlab_project=None,
            gitlab_access_token=None,
            gitlab_registration_token=None,
            debug=False,
            **kwargs
            ):
        super().__init__(logger_name='gl', **kwargs)
        if gitlab_access_token is None:
            raise RuntimeError("missing gitlab_access_token")
        self.gitlab_base = gitlab_base
        self.gitlab_project = gitlab_project
        self.gitlab_access_token = gitlab_access_token

    def api_get_paginated(self, ep):
        headers = {
                'PRIVATE-TOKEN': self.gitlab_access_token,
        }

        self.logger.debug("GET " + ep)
        url = self.gitlab_base + ep
        headers["pagination"]='keyset'
        while url is not None:
            req = requests.get(url, headers=headers)
            for u in json.loads(req.content):
                yield u

            links = req.headers["Link"].split(", ")
            links = [x.split("; ") for x in links ]
            links = {x[1]:re.sub("^<(.*)>$", lambda x: x.group(1), x[0]) for x in links}
            url = links.get('rel="next"')


    def api_get(self, ep):
        """ cannot be the same call as api_get_paginated, because it
        becomes a nightmare if we want something that works the same way
        for both arrays and dicts """
        headers = {
                'PRIVATE-TOKEN': self.gitlab_access_token,
        }

        self.logger.debug("GET " + ep)
        url = self.gitlab_base + ep
        req = requests.get(self.gitlab_base + ep, headers=headers)
        
        return json.loads(req.content)

    def api_post(self, ep, **kwargs):
        headers = {
                'PRIVATE-TOKEN': self.gitlab_access_token,
        }
        self.logger.debug("POST " + ep)
        req = requests.post(self.gitlab_base + ep, headers=headers, json=kwargs)
        return json.loads(req.content)

    def api_put(self, ep, **kwargs):
        headers = {
                'PRIVATE-TOKEN': self.gitlab_access_token,
        }
        self.logger.debug("PUT " + ep)
        req = requests.put(self.gitlab_base + ep, headers=headers, json=kwargs)
        return json.loads(req.content)

    def api_delete(self, ep, **kwargs):
        headers = {
                'PRIVATE-TOKEN': self.gitlab_access_token,
        }
        self.logger.debug("DELETE " + ep)
        req = requests.delete(self.gitlab_base + ep, headers=headers, json=kwargs)
        # runner deletion, when it succeeds, returns nothing.
        if req.content:
            return json.loads(req.content)
        else:
            return {}

    def pipelines(self, status=None):
        ep = f"/projects/{self.gitlab_project}"
        ep += f"/pipelines"
        if status is not None:
            ep += "?status=" + status
        return self.api_get_paginated(ep)

    def pipeline_details(self, pipeline_id):
        ep = f"/projects/{self.gitlab_project}"
        ep += f"/pipelines/{pipeline_id}"
        return self.api_get(ep)

    def pipeline_jobs(self, pipeline_id):
        ep = f"/projects/{self.gitlab_project}"
        ep += f"/pipelines/{pipeline_id}/jobs"
        return self.api_get_paginated(ep)

    def runners(self):
        ep = f"/projects/{self.gitlab_project}"
        ep += "/runners"
        return self.api_get_paginated(ep)

    def runner_delete(self, runner_id=None, runner_authentication_token=None):
        if runner_id is not None:
            ep = f"/runners/{runner_id}"
            return self.api_delete(ep)
        elif runner_authentication_token is not None:
            ep = f"/runners"
            return self.api_delete(ep, token=runner_authentication_token)
        else:
            raise RuntimeError("runner_delete wants either the runner_id or the runner_authentication_token")

    def runner_details(self, runner_id):
        return self.api_get(f"/runners/{runner_id}")

    def runner_pause(self, runner_id):
        return self.api_put(f"/runners/{runner_id}", active=False)

    def runner_resume(self, runner_id):
        return self.api_put(f"/runners/{runner_id}", active=True)

    def runner_jobs(self, runner_id):
        return self.api_get_paginated(f"/runners/{runner_id}/jobs")
