#!/usr/bin/env python3

from gl.no_boredom import no_boredom
import os
import json
import time
import requests
import base64
import subprocess
from urllib.parse import urlparse

SUBMITTED = 0
SCHEDULED = 1
RUNNING = 2
ACCESS = 3
OVER = 99999


class g5k_api(no_boredom):
    """This wraps a submission to the g5k jobs api, in such a way that
    the job is always cleaned on exit (unless the connection breaks,
    that is).
    """
    def __init__(self,
            g5k_base="https://api.grid5000.fr/3.0/",
            g5k_properties={},
            g5k_site="nancy",
            g5k_root_access=False,
            ssh_public_key_files=None,
            ssh_private_key_file=None,
            submission_access_timeout=180,
            g5k_login=None,
            g5k_password=None,
            g5k_read_from_netrc=False,
            **kwargs
            ):
        self.status = None
        super().__init__(logger_name='g5k', **kwargs)
        if g5k_root_access and ssh_public_key_files is None:
            raise RuntimeError("missing ssh pubkey info (mandatory for root)")
        if not g5k_read_from_netrc:
            if g5k_login is None:
                raise RuntimeError("missing g5k_login")
            if g5k_password is None:
                raise RuntimeError("missing g5k_password")
        self.g5k_login = g5k_login
        self.g5k_password = g5k_password
        self.g5k_base = g5k_base
        self.g5k_properties = g5k_properties
        self.g5k_site = g5k_site
        self.ssh_public_key_files = ssh_public_key_files
        self.submission_access_timeout = submission_access_timeout
        self.scheduled_at = None
        self.nodes = None
        # This is the submission json at some point, and soon becomes the
        # running json.
        self.j = {}
        self.jobid = None
        if g5k_read_from_netrc:
            N = netrc.netrc()
            login, account, password = N.hosts[urlparse(self.g5k_base).netloc]
            if g5k_login is None:
                self.g5k_login = login
            if g5k_password is None:
                self.g5k_password = password
        self.auth = (self.g5k_login, self.g5k_password)
        self.command_lines=[]
        self.command_lines.append("set -e")
        self.g5k_root_access = g5k_root_access
        self.ssh_private_key_file = ssh_private_key_file
        self.preparation_steps = []

        if g5k_root_access:
            self.command_lines.append("sudo-g5k")
            for d in self.ssh_public_key_files:
                self.command_lines.append("sudo tee -a /root/.ssh/authorized_keys <<'EOF'")
                self.command_lines += [s.strip() for s in open(d, "r").readlines()]
                self.command_lines.append("EOF")
        else:
            # Then we'll hope that we can simply connect with ssh!
            self.g5k_properties.setdefault("types", [])
            self.g5k_properties["types"].append("allow_classic_ssh")

    def add_job_preparation_step(self, name, preparation_code, verification_code):
        self.preparation_steps.append((name, verification_code))
        self.command_lines += preparation_code.split("\n")

    def accessor_script(self):
        self.command_lines.append("sleep 999999d")
        full = "".join([s+"\n" for s in self.command_lines]).encode("ascii")
        bb = base64.b64encode(full)
        return "echo {} | base64 -d | bash".format(bb.decode("ascii"))

    def api_get(self, ep, **kwargs):
        url = self.g5k_base + ep
        self.logger.debug("GET " + ep)
        return requests.get(self.g5k_base + ep, auth=self.auth, **kwargs)

    def api_post(self, ep, **kwargs):
        url = self.g5k_base + ep
        self.logger.debug("POST " + ep)
        return requests.post(self.g5k_base + ep, auth=self.auth, **kwargs)

    def api_delete(self, ep, **kwargs):
        url = self.g5k_base + ep
        self.logger.debug("DELETE " + ep)
        return requests.delete(self.g5k_base + ep, auth=self.auth, **kwargs)

    def submit_job(self):
        script = self.accessor_script()
        data = { "command": script, }
        data.update(self.g5k_properties)
        req = self.api_post(f"/sites/{self.g5k_site}/jobs", json=data)
        self.j = json.loads(req.content)
        self.jobid = self.j['uid']
        self.status = SUBMITTED
        p = f"{time.asctime()} : job {self.jobid} "
        self.info(p + "submitted")
        return self.j

    def delete_job(self):
        if self.status is None:
            return
        p = f"{time.asctime()} : job {self.jobid} "
        self.error(p + "being deleted")
        req = self.api_delete(f"/sites/{self.g5k_site}/jobs/{self.jobid}")
        self.status = None
        return req.content

    # TODO: do we really need this ?
    def __del__(self):
        self.delete_job()

    def run_command(self, cmd, interactive=False, stdin=None, may_fail=False):
        """ returns a subprocess.CompletedProcess """
        nodes = self.nodes
        if not nodes:
            raise RuntimeError("Attempting to do run_command without nodes!")

        ssh = ["ssh", "-oPasswordAuthentication=no"]

        if self.ssh_private_key_file is not None:
            ssh += [ "-i", self.ssh_private_key_file ]

        if interactive:
            ssh += [ "-t" ]

        user = 'root' if self.g5k_root_access else self.g5k_login
        ssh.append(f"{user}@{nodes[0]}")


        full = ssh + cmd.split(" ")

        if not interactive:
            kwargs=dict(stdin=subprocess.DEVNULL)
            if stdin is not None:
                kwargs['stdin'] = stdin
                kwargs['encoding'] = 'ascii'
            pipe = subprocess.run(full,
                    capture_output=True,
                    **kwargs)
            if pipe.returncode != 0:
                p = f"{time.asctime()} : job {self.jobid} "
                msg = p + "Command failed: " + cmd + "\n" + pipe.stderr.decode('ascii')
                if not may_fail:
                    raise RuntimeError(msg)
                else:
                    self.logger.debug(msg)
            return pipe
        else:
            rc = os.spawnvpe(os.P_WAIT, full[0], full, os.environ)
            return subprocess.CompletedProcess(None, rc)

    def check_job_scheduled(self, j):
        """This may be checked several times even once we know that the
        job has been scheduled, since the scheduled time might move."""

        if self.status >= RUNNING:
            return True

        scheduled_at = j.get('scheduled_at')
        if not scheduled_at:
            return False

        scheduled_at = int(scheduled_at)

        p = f"{time.asctime()} : job {self.jobid} "

        if scheduled_at is not self.scheduled_at:
            self.scheduled_at = scheduled_at
            when = time.asctime(time.localtime(self.scheduled_at))
            delta = self.scheduled_at - time.time()
            if delta < 0:
                when += " ({} seconds ago)".format(int(-delta))
            else:
                when += " (in {} seconds)".format(int(delta))
            self.info(p + "should run on " + when)

        if self.status < SCHEDULED:
            self.status = SCHEDULED
            self.info(p + "scheduled")

        return True

    def check_job_running(self, j):
        if self.status >= RUNNING:
            return True

        if j['state'] in [ 'tolaunch', 'launching', 'waiting' ]:
            return False

        nodes = j.get('assigned_nodes')
        if not nodes:
            return reached, j

        self.nodes = nodes

        p = f"{time.asctime()} : job {self.jobid} "
        self.info(p + f"running on nodes: {self.nodes}")

        self.status = RUNNING

        return True

    def check_job_access(self, j):
        if self.status >= ACCESS + len(self.preparation_steps):
            return True
        if self.status < ACCESS:
            b = self.run_command(':', may_fail=True).returncode == 0
            if not b:
                return False
            self.status = ACCESS
            p = f"{time.asctime()} : job {self.jobid} "
            self.info(p + f"reachable with ssh")
        for i in range(len(self.preparation_steps)):
            if self.status > ACCESS + i:
                continue
            name, verifier = self.preparation_steps[i]
            b = self.run_command(verifier, may_fail=True).returncode == 0
            if not b:
                return False
            self.status = ACCESS + i + 1
            p = f"{time.asctime()} : job {self.jobid} "
            self.info(p + f"done installing {name}")
        return True

    def check_job_error(self, j):
        if self.status == OVER:
            return True
        if j['state'] != 'error':
            return False

        p = f"{time.asctime()} : job {self.jobid} "

        self.error(p +"in Error state")
        for e in sorted(j['events'], key=lambda x:int(x['uid'])):
            when = time.asctime(int(e['created_at']))
            self.error(when + " " + e['description'])

        self.status = OVER
        return True

    def check_job(self):

        if self.status == OVER:
            return
    
        req = self.api_get(f"/sites/{self.g5k_site}/jobs/{self.jobid}")
        j = json.loads(req.content)

        if self.check_job_error(j):
            return

        p = f"{time.asctime()} : job {self.jobid} "

        self.debug_or_info(p + j['state'])

        if not self.check_job_scheduled(j):
            if self.status >= SCHEDULED:
                raise RuntimeError("should not happen")
            return

        if not self.check_job_running(j):
            return

        if j['state'] != 'running':
            self.status = OVER
            return

        # This checks all steps.
        if not self.check_job_access(j):
            return

        self.j = j

        return

    def check_job_still_ok(self):
        req = self.api_get(f"/sites/{self.g5k_site}/jobs/{self.jobid}")
        j = json.loads(req.content)
        return j['state'] == 'running' and self.run_command('test -f /ok-runner', may_fail=True).returncode == 0

    def explain_status(self):
        p = f"{time.asctime()} : job {self.jobid} "
        if self.status == SUBMITTED:
            self.info(p + "submitted")
        elif self.status == SCHEDULED:
            self.info(p + "launching")
        elif self.status == RUNNING:
            self.info(p + "setting up access")
        elif self.status == OVER:
            self.info(p + "dead (or dying)")
        elif self.status >= ACCESS + len(self.preparation_steps):
            self.info(p + "ready")
        else:
            for i in range(len(self.preparation_steps)):
                if self.status == ACCESS + i:
                    self.info(p + "installing " + self.preparation_steps[i][0])
                    return
            raise RuntimeError(p + f"reaches unexpected status {self.status}")

    def wait_for_access(self):
        submitted_at = time.time()
        deadline = submitted_at + self.submission_access_timeout
        while time.time() < deadline:
            if self.status >= ACCESS + len(self.preparation_steps):
                return True

            p = f"{time.asctime()} : job {self.jobid} "
            old = self.status
            self.check_job()
            if self.status > old:
                self.explain_status()
            if self.scheduled_at is not None:
                sched = self.scheduled_at
                if sched >= deadline:
                    # bailing out.
                    when = time.asctime(time.localtime(sched))
                    self.error(p + f"scheduled to start on {when}, refusing to wait")
                    break
            if self.status == OVER:
                self.error(p + "failed")
                break
            time.sleep(1)

        p = f"{time.asctime()} : job {self.jobid} "
        self.error(p + " did not / will not start in time, removing")
        self.delete_job()

        return False

if __name__ == '__main__':

    from config import read_config

    j = read_config()

    g5k = g5k_api(debug=True, **j)
    try:
        g5k.submit_job()
        g5k.wait_for_access()
        g5k.run_command("bash", interactive=True)
    finally:
        g5k.delete_job()
