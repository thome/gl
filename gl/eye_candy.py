import logging

class ANSI(object):
    """ Class defining some ANSI control sequences, for example for
    changing text color """
    CSI = '\x1b[' # ANSI Control Sequence Introducer. Not the TV show
    OSC = '\x1b]8'
    SGR = 'm' # Set Graphics Rendition code
    NORMAL = CSI + '0' + SGR
    BLACK = CSI + '30' + SGR
    GREY = CSI + '30;1' + SGR
    RED = CSI + '31' + SGR
    BRIGHTRED = CSI + '31;1' + SGR
    GREEN = CSI + '32' + SGR
    BRIGHTGREEN = CSI + '32;1' + SGR
    YELLOW = CSI + '33' + SGR
    BRIGHTYELLOW = CSI + '33;1' + SGR
    BLUE = CSI + '34' + SGR
    BRIGHTBLUE = CSI + '34;1' + SGR
    MAGENTA = CSI + '35' + SGR
    BRIGHTMAGENTA = CSI + '35;1' + SGR
    CYAN = CSI + '36' + SGR
    BRIGHTCYAN = CSI + '36;1' + SGR
    # There is another grey with code "37" - white without intensity
    # Not sure if it is any different from "30;1" aka "bright black"
    WHITE = CSI + '37;1' + SGR
    def hyperlink(target, text=None, width=None):
        if text is None:
            text = target
        text = str(text)
        pad = ''
        if width is not None:
            text = text[:width]
            pad = ' ' * (width - len(text))
        return "{0};;{1}\x07{2}{0};;\x07".format(ANSI.OSC, target, text) + pad

class ScreenFormatter(logging.Formatter):
    """ Class for formatting logger records for screen output, optionally
    with colorized logger level name (like cadofct.pl used to). """
    colors = {
        logging.DEBUG : ANSI.BLUE,
        logging.INFO : ANSI.BRIGHTGREEN,
        logging.WARNING : ANSI.BRIGHTYELLOW,
        logging.ERROR : ANSI.BRIGHTRED,
        logging.FATAL : ANSI.BRIGHTRED,
    }

    # Format string that switches to a different color (with ANSI code
    # specified in the 'color' key of the log record) for the log level name,
    # then back to default text rendition (ANSI code in 'nocolor')
    colorformatstr = \
        '%(padding)s%(color)s%(levelnametitle)s%(nocolor)s:%(name)s: %(message)s'
    # Format string that does not use color changes
    nocolorformatstr = \
        '%(padding)s%(levelnametitle)s:%(name)s: %(message)s'

    def __init__(self, color = True):
        self.usecolor = color
        if color:
            super().__init__(fmt=self.colorformatstr)
        else:
            super().__init__(fmt=self.nocolorformatstr)

    def format(self, record):
        # Add attributes to record that our format string expects
        if self.usecolor:
            record.color = self.colors.get(record.levelno, ANSI.NORMAL)
            record.nocolor = ANSI.NORMAL
        record.levelnametitle = record.levelname.title()
        if hasattr(record, "indent"):
            record.padding = " " * record.indent
        else:
            record.padding = ""
        return super().format(record)

class ScreenHandler(logging.StreamHandler):
    def __init__(self, lvl = logging.INFO, color = True, **kwargs):
        super().__init__(**kwargs)
        self.setLevel(lvl)
        self.setFormatter(ScreenFormatter(color = color))

