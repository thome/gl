#!/usr/bin/env python3

import os
import re
import sys
import time
import uuid
from urllib.parse import urlparse,urlunparse

from gl.no_boredom import no_boredom
from gl.gl_api import gl_api
from gl.g5k_api import g5k_api

# To run 4 provisioners in parallel:
#
# parallel -u --termseq INT,1000,HUP,1000,TERM,200,KILL,25 gl runner provision ::: {1..4}
#
# and no, --lb doesn't work because that seems to get in the way of the
# term sequence handling.

class Provisioner(no_boredom, object):
    def __init__(self,
            gitlab_registration_token=None,
            docker_image='gcc',
            runner_tags=[],
            **kwargs
            ):
        super().__init__(logger_name='provisioner', **kwargs)
        if gitlab_registration_token is None:
            raise RuntimeError("missing gitlab_registration_token")
        self.g5k = g5k_api(g5k_root_access=True, **kwargs)
        self.gl = gl_api(**kwargs)
        self.gitlab_registration_token = gitlab_registration_token
        self.runner_tags = runner_tags
        self.runner_id = None
        self.runner_description = None
        self.runner_authentication_token = None
        self.docker_image = docker_image
        self.all_jobs = {}
        self.g5k.add_job_preparation_step("docker", r"""
curl -sSL https://get.docker.com/ | sh +e
sudo mkdir /tmp/docker || :
sudo mkdir /var/lib/docker || :
sudo mount --bind /tmp/docker /var/lib/docker
sudo tee /etc/docker/daemon.json <<'EOF'
{ "registry-mirrors": ["http://docker-cache.grid5000.fr"] }
EOF
sudo systemctl restart docker
sudo touch /ok-docker
            """,
            "test -f /ok-docker")

        self.g5k.add_job_preparation_step("gitlab_runner", r"""
sudo curl -fsSL https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 -o /usr/local/bin/gitlab-runner
sudo chmod 755 /usr/local/bin/gitlab-runner
# glr is out of testing at the moment...
# export DEBIAN_FRONTEND=noninteractive
# apt-get -y update
# sudo apt-get -y install gitlab-runner
sudo touch /ok-runner
            """,
            "test -f /ok-runner")

    def provision(self):
        self.g5k.submit_job()
        if not self.g5k.wait_for_access():
            return False
        self.register_runner()
        return True
    
    def delete_runner(self):
        if self.runner_authentication_token is None:
            return
        d = self.runner_description
        if self.runner_id is not None:
            d += f" ({self.runner_id})"
        p = f"{time.asctime()} : runner {d} "
        self.error(p + "being deleted")
        if self.runner_id is not None:
            self.gl.api_delete(f"/runners/{self.runner_id}")
        else:
            self.gl.api_delete(f"/runners", token=self.runner_authentication_token)
        self.runner_authentication_token = None
        self.runner_id = None

    def unprovision(self):
        self.g5k.delete_job()
        self.delete_runner()

    def __del__(self):
        self.unprovision()

    def register_runner(self):
        assert self.gitlab_registration_token is not None
            
        gitlab_url = urlunparse(list(urlparse(self.gl.gitlab_base))[:2]+['']*4)

        node = re.sub(r'\..*', '', self.g5k.nodes[0])
        runner_description = "{}:{}".format(node, uuid.uuid4())
        self.runner_description = runner_description

        cmd = "/usr/local/bin/gitlab-runner register -n"
        cmd += f" --url {gitlab_url}"
        cmd += f" --registration-token {self.gitlab_registration_token}"
        cmd += f" --description {runner_description}"
        cmd +=  " --executor docker"
        cmd += f" --docker-image {self.docker_image}"
        cmd +=  " --run-untagged"
        if self.runner_tags:
            cmd += " --tag-list " + ",".join(self.runner_tags)

        pipe = self.g5k.run_command(cmd)

        p = f"{time.asctime()} : job {self.g5k.jobid} "
        self.info(p + "runner registered successfully, " + pipe.stdout.decode('ascii') + pipe.stderr.decode('ascii'))

        pipe = self.g5k.run_command(f"grep token /etc/gitlab-runner/config.toml")

        data = re.search(r'token = "([\S"]+)"', pipe.stdout.decode('ascii'))
        if not data:
            raise RuntimeError("no runner authentication token found!")
        self.runner_authentication_token = data.group(1)
        p = f"{time.asctime()} : job {self.g5k.jobid} "
        self.info(p + f"runner {runner_description} has authentication token {self.runner_authentication_token}")

        # we don't have systemd integration with the standalone binary.
        pipe = self.g5k.run_command(f"screen -dm /usr/local/bin/gitlab-runner run")

        for i in range(10):
            self.logger.debug("Searching for {} in active gitlab runners".format(runner_description))
            for r in self.gl.runners():
                description = r.get('description','')
                status = r.get('status','unknown')
                if description == self.runner_description:
                    p = f"{time.asctime()} : job {self.g5k.jobid} "
                    self.runner_id = r.get('id')
                    self.info(p + f"runner {runner_description} has id {self.runner_id}, and status \"{status}\"")
                    return
        self.logger.warning(p + f"Could not find the runner id for {runner_description}. Not a problem in some cases, but undoubtedly annoying")

    def list_new_jobs(self):
        p = f"{time.asctime()} : job {self.g5k.jobid} "
        if self.runner_authentication_token is None:
            self.error(p + "Cannot list jobs without a runner set up!")
        if self.runner_id is None:
            self.error(p + "Cannot list jobs if the runner id is unknown")

        now_all_jobs = list(self.gl.runner_jobs(self.runner_id))
        now_all_jobs = { (r['id'], r['status']) : r for r in now_all_jobs }

        for jk in set(now_all_jobs) - set(self.all_jobs):
            r = now_all_jobs[jk]
            print("{:10} {} {:10} {}".format(r['id'], r['project']['name'], r['status'], r['web_url']))

        self.all_jobs = now_all_jobs

    def is_still_ok(self):
        p = f"{time.asctime()} : job {self.g5k.jobid} "
        if not self.g5k.check_job_still_ok():
            self.logger.info("job is not ok")
            return False
        r = self.gl.runner_details(self.runner_id)
        if not r.get('online'):
            self.logger.info("runner is not online")
            return False
        if not r.get('active'):
            self.logger.info("runner is not active")
            return False
        return True

if __name__ == '__main__':
    from gl.config import read_config
    gp = Provisioner(**read_config())
    try:
        if gp.provision():
            gp.info("now sleeping a bit")
            while gp.is_still_ok():
                gp.list_new_jobs()
                time.sleep(10)
    finally:
        gp.unprovision()
