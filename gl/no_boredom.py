#!/usr/bin/env python3

import time
import logging
from .eye_candy import ScreenHandler

class no_boredom(object):
    def __init__(self, logger_name, debug=False, **kwargs):
        self.print_delay = 1
        self.next_print = time.time()
        self.logger = logging.getLogger(logger_name)
        loglevel = logging.DEBUG if debug else logging.INFO
        self.logger.addHandler(ScreenHandler(loglevel))
        self.logger.setLevel(loglevel)

    def info(self, *args):
        self.logger.info(*args)
        self.next_print = time.time()
        self.print_delay = 1

    def error(self, *args):
        self.logger.error(*args)
        self.next_print = time.time()
        self.print_delay = 1

    def debug(self, *args):
        self.logger.debug(*args)

    def debug_or_info(self, *args):
        if time.time() > self.next_print:
            self.logger.info(*args)
            self.next_print += self.print_delay
            self.print_delay *= 2
        else:
            self.logger.debug(*args)


if __name__ == '__main__':
    logger = no_boredom('main', debug=True)
    logger.debug("Hello!")
