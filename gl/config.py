#!/usr/bin/env python3

import json
import os
import re
import subprocess

def read_config(configfile=None):
    defaults = [
            "./gl.json.gpg",
            "./gl.json",
            os.path.join(os.environ.get("HOME"), "gl.json.gpg"),
            os.path.join(os.environ.get("HOME"), "gl.json"),
    ]
    if configfile is None:
        for d in defaults:
            if os.path.exists(d):
                configfile = d
                break
    if configfile is None:
        raise RuntimeError("No config file to fetch config data from!")

    try:
        if re.search(r'.json.gpg$', configfile):
            pipe = subprocess.run(["gpg", "-d", configfile], capture_output=True)
            if pipe.returncode != 0:
                raise RuntimeError("gpg decryption failed, cannot read " + configfile)
            return json.loads(pipe.stdout)
        elif re.search(r'.json$', configfile):
            return json.load(open(configfile, 'r'))
        else:
            raise RuntimeError("config file must be either .json or .json.gpg")
    except json.JSONDecodeError as e:
        print("JSON parsing error, your config file {} is probably incorrect".format(configfile))
        raise e



if __name__ == '__main__':
    print(json.dumps(read_config(), indent=2))
