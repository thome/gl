#!/usr/bin/env python3

import sys
import getopt
from gl import g5k_api,read_config
import json
from collections import Counter


def shell(configfile=None, args=[]):
    g5k = g5k_api(**read_config(configfile))
    try:
        g5k.submit_job()
        g5k.wait_for_access()
        g5k.run_command("bash", interactive=True)
    finally:
        g5k.delete_job()

def free(configfile=None, args=[]):
    g5k = g5k_api(**read_config(configfile))
    cluster = args[0] if args else 'grvingt'
    ep = f"/sites/{g5k.g5k_site}/clusters/{cluster}/status"
    j = json.loads(g5k.api_get(ep).content)
    print(Counter([v['soft'] for v in j['nodes'].values()]))


def print_help():
    help_text="""
Usage: g5k.py [[options]] [command] [[command_args ...]]

Recognized options:
    -c, --config <config file (.json or .json.gpg)
"""
    print(help_text)

if __name__ == '__main__':

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                "C:",
                [
                    "config=",
                    ])
    except getopt.GetoptError as e:
        print(e)
        print_help()
        sys.exit(1)

    configfile = None

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-C", "--config"):
            configfile = arg
            debug=True

    if len(args) == 0:
        print_help()
        sys.exit(1)

    if args[0] == 'help':
        print_help()
        sys.exit(0)
    elif args[0] == 'shell':
        shell(configfile, args[1:])
    elif args[0] == 'free':
        free(configfile, args[1:])

