#!/usr/bin/env python3

import sys
import getopt
import datetime
from gl import gl_api,read_config,Provisioner,eye_candy
import json
import re
import time
import logging
from collections import Counter

# To run 4 provisioners in parallel:
#
# parallel -u --termseq INT,1000,HUP,1000,TERM,200,KILL,25 ./gl.py runner provision ::: {1..4}
#
# and no, --lb doesn't work because that seems to get in the way of the
# term sequence handling.

def ptime(x):
    """parse dates as returned by gl api"""
    if x is None:
        return x
    else:
        x = re.sub(r'Z$','000Z',x)
        pat = r"%Y-%m-%dT%H:%M:%S.%fZ"
        return datetime.datetime.strptime(x, pat).timestamp()

def pdelta(x):
    """print time differences in minutes and seconds"""
    return "{}:{:02}".format(int(x)//60,int(x)%60)


def runner(configfile=None, args=[]):
    cfg = read_config(configfile)
    if args and re.match(r'prov(ision)?', args[0]):
        if args[1:]:
            logging.getLogger('toplevel').warning('discarded arguments: ' + ' '.join(args[1:]))
        gp = Provisioner(**cfg)
        try:
            if gp.provision():
                gp.info("now sleeping a bit")
                while gp.is_still_ok():
                    gp.list_new_jobs()
                    time.sleep(10)
        finally:
            gp.unprovision()
        return

    gl = gl_api(**cfg)
    if len(args) == 0:
        for r in gl.runners():
            id = r.get('id')
            s = r.get('status')
            description = r.get('description')
            if s == 'online':
                s = f'{s:8}'
                s = eye_candy.ANSI.GREEN + s + eye_candy.ANSI.NORMAL
            elif s == 'offline':
                s = f'{s:8}'
                s = eye_candy.ANSI.BRIGHTRED + s + eye_candy.ANSI.NORMAL
            elif s == 'paused':
                s = f'{s:8}'
                s = eye_candy.ANSI.YELLOW + s + eye_candy.ANSI.NORMAL
            else:
                s = f'{s:8}'
            print("{:6} {} {}".format(id, s, description))
    elif re.match(r'del(ete)?', args[0]):
        for id in args[1:]:
            if re.match(r'\d+', id):
                gl.runner_delete(runner_id=int(id))
            else:
                gl.runner_delete(runner_authentication_token=id)
    elif args[0] == 'pause':
        for id in args[1:]:
            gl.runner_pause(id)
    elif args[0] == 'resume':
        for id in args[1:]:
            gl.runner_resume(id)
    elif args[0] == 'details':
        for id in args[1:]:
            print(json.dumps(gl.runner_details(id), indent=2))
    else:
        if args[0] == 'jobs':
            args = args[1:]
        for rid in args[0:]:
            if not re.match(r'\d+', rid):
                raise RuntimeError("garbage in command: " + rid)
            for r in list(gl.runner_jobs(int(rid)))[-20:]:
                f = ptime(r.get('finished_at'))
                sta = ptime(r.get('started_at'))
                # if f is not None and time.time() > f + 86400:
                #     continue
                td = ''
                if f is not None and sta is not None:
                    td = str(pdelta(f - sta))
                td = f'{td:6}'

                id = r.get('id')
                s = r.get('status','')
                n = r.get('name','')[-25:]
                if s == 'success':
                    s = f"{s:10.10}"
                    s = eye_candy.ANSI.GREEN + s + eye_candy.ANSI.NORMAL
                elif s == 'failed':
                    s = f"{s:10.10}"
                    s = eye_candy.ANSI.BRIGHTRED + s + eye_candy.ANSI.NORMAL
                elif s == 'canceled':
                    s = f"{s:10.10}"
                    s = eye_candy.ANSI.YELLOW + s + eye_candy.ANSI.NORMAL
                elif s == 'running':
                    s = f"{s:10.10}"
                    s = eye_candy.ANSI.BLUE + s + eye_candy.ANSI.NORMAL
                else:
                    s = f"{s:10.10}"
                # web_url = r.get('web_url')
                w = eye_candy.ANSI.hyperlink(
                        r.get('web_url'),
                        text=id, width=10)
                pj = r.get('project',{}).get('name')
                message = r.get('commit',{}).get('message','')
                message = message.split('\n')[0][:30]
                print("{} {:25} {} {} {} {}".format(w, n, pj, s, td, message))

def pipeline(configfile=None, args=[]):
    cfg = read_config(configfile)
    gl = gl_api(**cfg)
    pls = []
    if len(args) == 1 and re.match(r'[a-z]+', args[0]):
        if args[0] == 'all':
            pls = list(gl.pipelines())
        else:
            pls = list(gl.pipelines(status=args[0]))
    elif args:
        for rid in args:
            if not re.match(r'\d+', rid):
                raise RuntimeError("garbage in command: " + rid)
            pls.append(gl.pipeline_details(int(rid)))
    else:
        pls = list(gl.pipelines(status='running'))

    for pl in pls:
        id = pl.get('id')
        created_at = pl.get('created_at')
        updated_at = pl.get('updated_at')
        ref = pl.get('ref')
        web_url = pl.get('web_url','')
        s = pl.get('status')
        if s:
            if s == 'success':
                s = eye_candy.ANSI.GREEN + s + eye_candy.ANSI.NORMAL
            elif s == 'failed':
                s = eye_candy.ANSI.BRIGHTRED + s + eye_candy.ANSI.NORMAL
            elif s == 'canceled':
                s = eye_candy.ANSI.GREY + s + eye_candy.ANSI.NORMAL
            elif s == 'running':
                s = eye_candy.ANSI.BLUE + s + eye_candy.ANSI.NORMAL
        print("{} ({}) created {}, updated {} ; {}".format(eye_candy.ANSI.hyperlink(web_url, text=id), s, created_at, updated_at, ref))

        for r in gl.pipeline_jobs(id):
            n = r.get('name','')[-30:]
            s = r.get('status','')
            # w = r.get('web_url')
            w = eye_candy.ANSI.hyperlink(r.get('web_url'), text=r.get('id'))
            if s == 'success':
                s = '{:8}'.format(s)
                s = eye_candy.ANSI.GREEN + s + eye_candy.ANSI.NORMAL
            elif s == 'failed':
                s = '{:8}'.format(s)
                s = eye_candy.ANSI.BRIGHTRED + s + eye_candy.ANSI.NORMAL
            elif s == 'running':
                s = '{:8}'.format(s)
                s = eye_candy.ANSI.BLUE + s + eye_candy.ANSI.NORMAL
            elif s == 'created':
                s = '{:8}'.format(s)
                s = eye_candy.ANSI.YELLOW + s + eye_candy.ANSI.NORMAL
            elif s == 'canceled':
                s = '{:8}'.format(s)
                s = eye_candy.ANSI.GREY + s + eye_candy.ANSI.NORMAL
            else:
                s = '{:8}'.format(s)
            if r.get('runner') is not None:
                i = r['runner'].get('id')
                d = r['runner'].get('description')[:16]
                print(" {:<30} {} on {:6}/{:16}; {}".format(n, s, i, d, w))
            else:
                print(" {:<30} {}    {:6} {:16}; {}".format(n, s, '', '',w))

def print_help():
    help_text="""
Usage: gl.py [[options]] [command] [[command_args ...]]

Recognized options:
    -c, --config <config file (.json or .json.gpg)

Commands:
    runner
    runner del [runner number ... ]
    runner pause [runner number ... ]
    runner resume [runner number ... ]
    runner provision
    pipelines [[pipeline number ... ]]
"""
    print(help_text)

if __name__ == '__main__':

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                "C:",
                [
                    "config=",
                    ])
    except getopt.GetoptError as e:
        print(e)
        print_help()
        sys.exit(1)

    configfile = None

    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-C", "--config"):
            configfile = arg
            debug=True

    if len(args) == 0:
        print_help()
        sys.exit(1)

    if args[0] == 'help':
        print_help()
        sys.exit(0)
    elif re.match(r'r(unners?)?', args[0]):
        runner(configfile, args[1:])
    elif re.match(r'p(ipe(line)?s?)?', args[0]):
        pipeline(configfile, args[1:])
    else:
        raise RuntimeError("unknown command: " + args[0])
